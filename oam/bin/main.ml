open Skoam

module M = Necromonads.List

module Types = struct end

module Spec = struct
  include Unspec(M)(Types)

  let string_of_var n =
    let rec aux acc = function
      | Z -> acc
      | S m -> aux (acc + 1) m
    in
    aux 0 n

  let pro b = if b then "(" else ""
  let prc b = if b then ")" else ""

  let rec string_of_term par = function
    | TLam t ->
      Printf.sprintf "%sλ.%s%s" (pro par) (string_of_term false t) (prc par)
    | TVar x -> Printf.sprintf "%d" (string_of_var x)
    | TApp (t1,t2) ->
      Printf.sprintf "%s%s @ %s%s"
        (pro par) (string_of_term true t1) (string_of_term true t2) (prc par)

  let string_of_annot = function
    | Emp -> ""
    | NEv -> "^"

  let rec string_of_env par = function
    | Id -> "id"
    | Shift -> "↑"
    | Cons (c,e) ->
      Printf.sprintf "%s%s :: %s%s"
        (pro par) (string_of_clos false c) (string_of_env false e) (prc par)
    | Comp (e1,e2) ->
      Printf.sprintf "%s%s o %s%s"
        (pro par) (string_of_env true e1) (string_of_env true e2) (prc par)
    | Lift e ->
      Printf.sprintf "%s⇧%s%s" (pro par) (string_of_env true e) (prc par)

  and string_of_clos par = function
    | CEnv (a,e,tc) ->
      Printf.sprintf "%s%s[%s]%s%s"
        (pro par) (string_of_tc true tc) (string_of_env false e) (string_of_annot a) (prc par)
    | CLam (a, c) ->
      Printf.sprintf "%sλ%s.%s%s"
        (pro par) (string_of_annot a) (string_of_clos false c) (prc par)
    | CApp (a, c1, c2) ->
      Printf.sprintf "%s%s @%s %s%s"
        (pro par) (string_of_clos true c1) (string_of_annot a)
        (string_of_clos true c2) (prc par)

  and string_of_tc par = function
    | T t -> string_of_term par t
    | C c -> string_of_clos par c

  and string_of_ctx = function
    | Bot -> "ε"
    | Lapp (c, ctx) ->
      Printf.sprintf "□ %s :: %s" (string_of_clos false c) (string_of_ctx ctx)
    | Rapp (c, ctx) ->
      Printf.sprintf "%s □ :: %s" (string_of_clos false c) (string_of_ctx ctx)
    | Lam ctx -> Printf.sprintf "λ :: %s" (string_of_ctx ctx)

  let string_of_eenv = function
    | Star -> "*"
    | Bang e -> Printf.sprintf "%s" (string_of_env false e)

  let string_of_conf = function
    | Ev (tc, ctx, eenv) ->
      Printf.sprintf "<%s | %s | %s>ev"
        (string_of_tc false tc) (string_of_ctx ctx) (string_of_eenv eenv)
    | Var (env, ctx, n, eenv, (n', env')) ->
      Printf.sprintf "<%s | %s | %d | %s | (%d, %s)>var"
        (string_of_env false env) (string_of_ctx ctx) (string_of_var n) (string_of_eenv eenv)
        (string_of_var n') (string_of_env false env')
    | BEv (ctx, clos) ->
      Printf.sprintf "<%s | %s>bev" (string_of_ctx ctx) (string_of_clos false clos)
    | Nf clos ->
      Printf.sprintf "<%s>nf" (string_of_clos false clos)
    | Rec (ctx, clos) ->
      Printf.sprintf "<%s | %s>rec" (string_of_ctx ctx) (string_of_clos false clos)

  let string_of_rule = function
    | EvEnv -> "EvEnv"
    | EvLappC -> "EvLappC"
    | EvLappT -> "EvLappT"
    | EvRappC -> "EvRappC"
    | EvRappT -> "EvRappT"
    | EvLamC -> "EvLamC"
    | EvLamT -> "EvLamT"
    | EvVarBang -> "EvVarBang"
    | EvBetaC -> "EvBetaC"
    | EvBetaT -> "EvBetaT"
    | EvVarBack -> "EvVarBack"
    | EvOther -> "EvOther"
    | VarS -> "VarS"
    | VarZ -> "VarZ"
    | VarId -> "VarId"
    | VarIdNot -> "VarIdNot"
    | VarShift -> "VarShift"
    | VarShiftNot -> "VarShift"
    | VarLiftS -> "VarLiftS"
    | VarLiftZ -> "VarLiftZ"
    | VarLiftNot -> "VarLiftNot"
    | VarComp -> "VarComp"
    | BevLam -> "BevLam"
    | BevLapp -> "BevLapp"
    | BevRapp -> "BevRapp"
    | BevNf -> "BevNf"
    | RecLapp -> "RecLapp"
    | RecRapp -> "RecRapp"
    | RecLam -> "RecLam"
    | RecEv -> "RecEv"

  let rec prlog = function
    | Empty -> ()
    | Single (cb, rule, ca) ->
      Printf.printf "%s  --%s->  %s\n" (string_of_conf cb) (string_of_rule rule) (string_of_conf ca)
    | Concat (l1, l2) -> prlog l1; prlog l2

  let prdeb (_,l) =
    Printf.printf "\nDEBUG\n";
    prlog l;
    M.ret ()
end

open Spec

open MakeInterpreter(Spec)


let prll ll =
  let nb = ref 0 in
  List.iter (fun (_,l) ->
      incr nb;
      Printf.printf "\n**Run %d**\n\n" !nb;
      prlog l)
    ll

let id = TLam (TVar Z)

let t0 = TLam (TApp (TVar Z, TLam (TVar (S Z))))

let t = TApp (TApp (t0, id), TVar (S Z))

let () = prll (evalt t)
